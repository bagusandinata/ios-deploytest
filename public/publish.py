# -*- coding: utf-8 -*-
"""
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Install {title}</title>

</head>
<body>
    <div class="main" style="margin:50px; text-align:center;">
        <h1>{title}</h1>
        <p>Version {version}</p>
        <p>Date {date}</p>
        <a href="itms-services://?action=download-manifest&url={base_url}/manifest.plist">INSTALL</a>
    </div>
</body>
</html>
"""

import sys
from plistlib import readPlist
from datetime import datetime

def main():
    base_url = sys.argv[1]
    plist_file = "root@128.199.221.113:/usr/share/nginx/html/android/manifest.plist"
    index_html_path = "public/index.html"
    read_plist_file = readPlist(plist_file)
    metadata = read_plist_file['items'][0]['metadata']
    info = dict()
    info["title"] = metadata['title']
    info["version"] = metadata['bundle-version']
    info["bundleid"] = metadata['bundle-identifier']
    info["base_url"] = base_url
    info["date"] = datetime.now()
    with open(index_html_path, "w+") as f:
        f.write(__doc__.format(**info))


if __name__ == '__main__':
    main()
